from django.contrib import admin

# Register your models here.


from v1.channels.models.channel import Channel


class ChannelAdmin(admin.ModelAdmin):
    list_display = ('name','owner','private')

admin.site.register(Channel, ChannelAdmin)