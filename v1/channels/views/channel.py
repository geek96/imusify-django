from django.http import Http404
from rest_framework import generics, permissions, status
from rest_framework.response import Response

from v1.channels.models.channel import Channel
from v1.channels.serializer.channel import ChannelSerializer
from v1.channels.serializer.channelpost import ChannelPostSerializer
from v1.posts.models.post import Post
from v1.posts.serializers.post import PostSerializer


class ChannelList(generics.ListCreateAPIView):

    model  = Channel
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer
    permissions_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        return Channel.objects.filter(owner=self.request.user)



class ChannelDetail(generics.RetrieveAPIView):
    model = Channel
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'id'

    def get_queryset(self):
        return Channel.objects.filter(owner=self.request.user)


class ChannelPosts(generics.ListAPIView):
    model = Post
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        try:
            channel_id = self.kwargs.get('id')
            return Post.objects.filter(channels__id=channel_id)
        except:
            raise Http404

class ChannelNewPost(generics.CreateAPIView):

    model = Post
    serializer_class = ChannelPostSerializer
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, id):

        post = request.data
        post['owner'] = self.request.user.id
        serializer = self.serializer_class(data=post)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        serializer.instance.channels.add(id)

        return Response(serializer.data, status=status.HTTP_201_CREATED)

