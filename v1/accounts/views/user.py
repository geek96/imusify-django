from rest_framework import generics, permissions

from v1.accounts.models.user import User
from v1.accounts.serializers.user import UserSerializer


class UserList(generics.ListCreateAPIView):

    model  = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permissions_classes = (permissions.AllowAny,)



class UserDetail(generics.RetrieveAPIView):
    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'username'

