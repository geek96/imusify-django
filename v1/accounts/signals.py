from django.conf import settings
from django.core.mail import EmailMessage
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string

from v1.accounts.models.user import User


@receiver(post_save, sender=User)
def account_verification(sender, instance, **kwargs):
    data = {
        'name': instance.get_full_name(),
        'username': instance.username,
        'code': instance.pk,
        'domain': 'http://localhost:8080/',
        'link': 'user/account/activate/'
    }
    body = render_to_string('templates/email/user_verification.html', data)
    email = EmailMessage(
        subject="Imusify :: Account Verification!",
        body=body,
        from_email=settings.FROM_EMAIL,
        reply_to=[settings.REPLY_TO],
        to=[instance.email],
        headers={
            'Message-ID': 'imusify'
        }
    )

    email.content_subtype = "html"
    email.send(fail_silently=False)
