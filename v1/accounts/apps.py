from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'v1.accounts'


    def ready(self):
        import v1.accounts.signals