from django.conf.urls import url

from v1.accounts.views import user, profile

urlpatterns = [
    url(r'^(?P<user__username>[0-9a-zA-Z_-]+)/profile$', profile.ProfileDetail.as_view(), name="userprofile-detail"),
    url(r'^(?P<username>[0-9a-zA-Z_-]+)$', user.UserDetail.as_view(), name='user-detail'),
    url(r'^$', user.UserList.as_view(), name="user-list")
]

