from django.db import models

from v1.general.models import TimedModel
from v1.general import utils

class Attachment(TimedModel):
    file = models.FileField(upload_to=utils.attachment_upload_to)
    content_type = models.CharField(max_length=150)
    data = models.TextField(blank=True)

