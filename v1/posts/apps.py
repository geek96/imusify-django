from django.apps import AppConfig


class PostsConfig(AppConfig):
    name = 'v1.posts'


    def ready(self):
        import v1.posts.signals