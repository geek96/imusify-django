from rest_framework import serializers

from v1.posts.models.attachment import Attachment


class AttachmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Attachment
        fields = ('id','file','content_type')