from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from v1.posts.views import tag
from v1.posts.views.attachment import AttachmentDetail,AttachmentNew
from v1.posts.views.vote import CastVoteView

postroutes = routers.DefaultRouter()
postroutes.register(r'tag', tag.TagDetail, base_name='tag-detail')


urlpatterns = [
    url(r'^', include(postroutes.urls)),
    url(r'^attachment/new', AttachmentNew.as_view() , name="attachment-detail"),
    url(r'^attachment/(?P<pk>\d+)$', AttachmentDetail.as_view() , name="attachment-detail"),
    url(r'^(?P<post_id>\d+)/vote/(?P<action>up|down)/$', CastVoteView.as_view() , name="castvote")

]
